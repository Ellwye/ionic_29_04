import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utilisateur } from '../../models/utilisateur-interface';
import { environnement } from './../../models/environnement';
import { map  } from 'rxjs/operators';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  utilisateurs : Utilisateur[];

  constructor(private http: HttpClient,
    private navCtrl: NavController,
    private router: Router) {
    this.loadUser();
  }

  ngOnInit() {
  }

  loadUser(): void {
    let url = `${environnement.api_url}/Utilisateurs`;
    console.log('url', url);
    this.http.get(url).pipe(
      map(
        (utilisateurs : Utilisateur[]) => this.utilisateurs = utilisateurs
      )
    ).subscribe(utilisateurs => console.log('utilisateurs', utilisateurs));
  }

  goToUser(id : string): void {
    this.router.navigate(['/user', id]);
  }

}
