import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../../models/utilisateur-interface';
import { environnement } from './../../models/environnement';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.page.html',
  styleUrls: ['./update-user.page.scss'],
})
export class UpdateUserPage implements OnInit {

  id : any;
  user = {} as Utilisateur;
  message : string;

  constructor(private http: HttpClient,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.user.username = "";
    this.user.avatar = "";
    this.user.password = "";
    this.user.mail = "";
    this.route.params.subscribe(params => {
      this.id = params['id']; 
    });
    console.log(this.id);
    this.loadUser();
  }

  loadUser(): void {
    let url = `${environnement.api_url}/Utilisateurs/${this.id}`;
    console.log('url', url);
    this.http.get(url).pipe(
      map(
        (user : any) => this.user = user
      )
    ).subscribe(user => console.log('user', user));
  }

  updateUser(): void {
    let url = `${environnement.api_url}/Utilisateurs/${this.id}`;
    this.http.patch(url, this.user)
    .subscribe(results => console.log('results', results));
    this.message = "L'utilisateur a bien été mis à jour.";
  }

}
