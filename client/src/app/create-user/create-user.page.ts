import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utilisateur } from '../../models/utilisateur-interface';
import { environnement } from './../../models/environnement';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.page.html',
  styleUrls: ['./create-user.page.scss'],
})
export class CreateUserPage implements OnInit {

  utilisateur = {} as Utilisateur;
  message : string;
  // button : boolean = false;

  constructor(private http: HttpClient,
  private navCtrl: NavController,
  private router: Router
  ) {
    this.loadUser();
    this.utilisateur.username = "";
    this.utilisateur.avatar = "";
  }

  ngOnInit() {}

  loadUser(): void {
    let url = `${environnement.api_url}/Utilisateurs`;
    console.log('url', url);
    this.http.get(url)
    .subscribe(utilisateur => console.log('utilisateurs', utilisateur));
    // this.navCtrl.navigateRoot('/users'); // REDIRECTION
  }

  insertUser(): void {
    let url = `${environnement.api_url}/Utilisateurs`;
    if (this.utilisateur.mail != null && this.utilisateur.password != null) {
      this.http.post(url, this.utilisateur)
      .subscribe(results => console.log('results', results))
    }
    this.message = "L'utilisateur " +this.utilisateur.username+" a bien été créé.";
    // this.button = true;
  }

  // goToUser(id : string): void {
  //   this.router.navigate(['/user', id]);
  // }

}
