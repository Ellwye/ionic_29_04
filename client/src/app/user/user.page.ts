import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environnement } from './../../models/environnement';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Utilisateur } from '../../models/utilisateur-interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {

  id: any;
  user = {} as Utilisateur;

  constructor(private http: HttpClient,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private router: Router) {
      this.user.username = "";
      this.user.avatar = "";
      this.user.mail = "";
      this.user.id = "";
     }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id']; 
    });
    console.log(this.id);
    this.loadUser();
  }

  loadUser(): void {
    let url = `${environnement.api_url}/Utilisateurs/${this.id}`;
    console.log('url', url);
    this.http.get(url).pipe(
      map(
        (user : any) => this.user = user
      )
    ).subscribe(user => console.log('user', user));
  }

  deleteUser(): void {
    let url = `${environnement.api_url}/Utilisateurs/${this.id}`;
    this.http.delete(url)
    .subscribe(results => console.log('results', results));
    this.router.navigate(['users']);
  }

  goToEditUser(id : string) {
    this.router.navigate(['/update-user', id]);
  }

}
